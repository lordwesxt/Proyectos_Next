import Link from "next/link";
import Image from "next/image";
import Styles from "../styles/Header.module.css";

const Header = () => {
  return (
    <header className={Styles.header}>
          
            <div className={Styles.navigation}>
              <Link href="/">
                  <div className={Styles.logo}>
                    <Image priority={true} width={100} height={100} src='/img/logo2.png' alt="Logo del header"/>
                  </div>
                </Link>
                <Link href="/characters" className={Styles.linkCharacters}>
                  <p>Personajes</p>
                </Link>
            </div>
    </header>
  )
}

export default Header