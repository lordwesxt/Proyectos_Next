import Link from "next/link"
import Image from "next/image"
import Styles from '../styles/CharactersElements.module.css'
import { useState, useEffect } from "react"

export default function CharactersElements({elements}) {
  const {id ,name, status, species, image} = elements
  const [isAlive, setIsAlive] = useState(false);
  const [isUndefined,setIsUndefined] = useState(false);

  useEffect(() => {
    if (status === 'Alive') {
      setIsAlive(true);
    } else {
      setIsAlive(false);
    }
    if(status === 'unknown'){
      setIsUndefined(true)
    }
  }, [status]);
  return (
  <Link href={`/onlyCharacters/${id}`} className={Styles.link_onlyCharacter}>
        <div className={Styles.content_grid}>
      <Image priority={true} width={300} height={300} className={Styles.image_character} src={image} alt={`imagen del personaje ${name}`}/>
      <div className={Styles.atributes}>
        <p className={Styles.name}>{name}</p>
        <span className={`${Styles.estado_atribute} ${isAlive ? Styles._alive: Styles._dead} ${isUndefined ? Styles._null : ''}` } id={"estado"}>{status== `unknown` ? `Desconocido` : status}</span>
        <span className={Styles.especies_atribute} id={"especies"}>{species}</span> 
        
      </div>
      
        <p className={Styles.link_leyend}>Ver más</p>
  </div>
  </Link>
  )
}



