import Styles from '../styles/Grid.module.css'

function Grid({elements, component: Component}) {
  if(!Array.isArray(elements)){
    return (
      <div className={Styles.Grid}>
    <Component elements={elements}/>
    </div>
    )
  }else{
    return (
    <div className={Styles.Grid}>
      {elements.map((item) => {
        return (
          <Component key={item.id} elements={item}/>
        )
      })}
    </div>
  )
  }
  
}




export default Grid