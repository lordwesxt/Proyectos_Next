import Image from 'next/image'
import Styles from '../styles/Loader.module.css'

export const Loader = () => {
  return (
    <div className={Styles.loader}>
        <Image src={'/img/rick-morty-in-pc.png'} width={500} height={500} priority={true} alt={'rick and morty in pc'}/>
    </div>
  )
}
