import React from 'react'
import Styles from '../styles/OnlyCharacterElement.module.css'
import { formatearFecha } from '../helpers';
import Link from 'next/link';
import Image from 'next/image';
const OnlyCharacterElement = ({elements}) => {
    const { name, status, species, type, gender, origin, location, episode, image, created } = elements;

  return (
    <>
        <div className={Styles.content} style={{background: `radial-gradient(49% 81% at 45% 47%, rgba(0, 0, 0, 0.32) 0%, rgba(7, 58, 255, 0) 100%) 0% 0% / cover, radial-gradient(113% 91% at 17% -2%, rgba(255, 213, 0, 0.9) 1%, rgba(255, 0, 0, 0) 99%), radial-gradient(142% 91% at 83% 7%, hsl(62deg 100% 40% / 88%) 1%, rgba(255, 0, 0, 0) 99%), radial-gradient(142% 91% at -6% 74%, hsl(139deg 100% 35% / 82%) 1%, rgba(255, 0, 0, 0) 99%), radial-gradient(142% 91% at 111% 84%, hsl(162deg 100% 25% / 75%) 0%, rgb(0, 0, 0) 100%), url(${image}) 0% 0%/cover`}}>
        <Image
          priority={true}
          width={300}
          height={300}
          className={Styles.image_character}
          src={image}
          alt={`imagen del personaje ${name}`}
        />
        <div className={Styles.atributes}>
          <div className={Styles.atributes_first}>
            <span className={Styles.sub}>Nombre:</span> <p>{name}</p>
            <span className={Styles.sub}>Genero:</span> <p>{gender}</p>
            <span className={Styles.sub}>Estado:</span> <p>{status}</p>
            <span className={Styles.sub}>Origen:</span> <p>{origin.name}</p>
            <span className={Styles.sub}>Localización:</span> <p>{location.name}</p>
            <span className={Styles.sub}>Creado en:</span> <p>{formatearFecha(created)}</p>
          </div>
          <div className={Styles.atributes_second}>
            <span className={Styles.sub}>Especie: </span> <p>{species}</p>
            <span className={Styles.sub}>Tipo:</span> <p>{type ? type : `N/A`}</p>
            <span className={Styles.sub}>Episode:</span> <div className={Styles.link_episodes}>{episode.map((item,index)=> {const numEpisode = item.split('/'); return (<Link key={index} href={item}>{'Episodio ' + numEpisode[5]}</Link>)})}</div>
          </div>
        </div>
      </div>
      </>
  )
}

export default OnlyCharacterElement