import Styles from '../styles/Footer.module.css'


function Footer() {
  return (
    <footer className={Styles.footer1}>
        <div className={Styles.content}>
            <h1>Arnold Beleño - Next.js</h1>
        </div>
    </footer>
  )
}

export default Footer