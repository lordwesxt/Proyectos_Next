import Header from "./Header"
import Footer from "./Footer"
import Head from "next/head"
import Styles from "../styles/Layout.module.css"

function Layout({children,titulo=''}) {

  return (
    <div>
      <Head>
        <title>{`Rick and Morty | ${titulo}`}</title>
      </Head>
      
        <Header />
        <div className={Styles.content}>
          {children}
        </div>
        <Footer />
      </div>
  )
}

export default Layout