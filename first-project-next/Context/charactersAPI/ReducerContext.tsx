
export const reducer = (state, action) => {
  const {payload, info, pageActual, errorLoad, type} = action
  switch(type){
    case 'characterGlobal':
      return {
        ...state,
        charactersGlobal: payload,
        info: info,
        pageActual: pageActual
      }
        case 'characterOnly':
        return {
          ...state,
          characterOnly: payload,
          errorLoad: errorLoad
        }
  }
}