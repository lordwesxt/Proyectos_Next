
import { useReducer } from "react";
import { reducer } from "./ReducerContext";
import { context } from "./CreateContexts";
import axios from "axios";


const StateContexts = ({ children }) => {
  const inicialState = {
    charactersGlobal: [],
    info: [],
    pageActual: [],
    characterOnly: [],
    errorLoad: null
  }
  const [state, dispatch] = useReducer(reducer, inicialState)
  

  let filterUrl = `${process.env.NEXT_PUBLIC_API_URL}/api/character`

  const characterGlobal = async (Url) => {
    if(Url !== filterUrl + '/?page=[]'){filterUrl = Url}
    let data
    await axios.get(filterUrl).then((response)=>{
      data = response.data
    })
    const pageActual = () => {
      let pageActualNum = 1
      if(data.info.prev){
      const pagePrev = data.info.prev.split('=')
        pageActualNum = Number.parseInt(pagePrev[1]) + 1
      }
      return pageActualNum
    }

    dispatch({
      payload: data.results,
      info: data.info,
      pageActual: pageActual(),
      type: `characterGlobal`
    })
  }

  const characterOnly = async (id) => {
    let data
    await axios.get(filterUrl + `/${id}`).then((response)=>{
      data = response.data
    })
    dispatch({
      payload: data,
      errorLoad: data.error,
      type: 'characterOnly'
    })
  }

  return (
    <context.Provider value={{ pj: state.charactersGlobal,
      info: state.info,
      pageActual: state.pageActual,
    isLoading: state.isLoading,
    onlyPj: state.characterOnly,
    errorLoad: state.errorLoad,
    characterGlobal, 
    characterOnly
     }}>
      {children}
    </context.Provider>
  )
}

export default StateContexts