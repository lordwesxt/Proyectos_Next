import Styles from "../styles/Charaters.module.css"
import Layout from "../components/Layout"
import Grid from "../components/Grid"
import CharactersElements from "../components/CharactersElements"
import { Loader } from "../components/Loader"
import { useContext, useEffect, useState } from "react"
import { context } from "../Context/charactersAPI/CreateContexts"

function Characters() {
  const {pj, info, pageActual, characterGlobal} = useContext(context)
  const [filterUrl,setFilterUrl] = useState(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=${pageActual}`)
  const [isLoading, setIsLoading] = useState(true)
  useEffect(()=>{
     characterGlobal(filterUrl)
     setIsLoading(false)
  },[filterUrl])
  if(isLoading){
    return (
      <Layout>
        <div className={Styles.title}>
          <h1>Personajes</h1>
        </div>
        <Loader/>
      </Layout>
    )
  }else {
      return (
    <Layout titulo={'Personajes'}>
      <div className={Styles.title}>
        <h1>Personajes</h1>
      </div>
        <Grid elements={pj} component={CharactersElements}/>
        <div className={Styles.btn_page_next_prev}>
          <button onClick={() =>{setFilterUrl(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=1`)}} disabled={!info.prev}>Primer pagina</button>
          <button onClick={() =>{setFilterUrl(info.prev)}} disabled={!info.prev}>Prev</button>
          <button onClick={() =>{setFilterUrl(info.next)}} disabled={!info.next}>Next</button>
          <button onClick={() =>{setFilterUrl(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=${info.pages}`)}} disabled={!info.next}>Ultima pagina</button>
        </div>
        <span className={Styles.pageActual}>Pagina: {pageActual}</span>
        </Layout>
  )
  }
}




export default Characters