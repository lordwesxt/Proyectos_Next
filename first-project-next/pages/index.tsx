
import Layout from "../components/Layout"
import Styles from "../styles/Index.module.css"
export default function Home() {
    return (
            <Layout 
            titulo={"Inicio"}>
                <main>
            <div className={Styles.introducción}>
                <h1>Introducción</h1>
                <div className={Styles.cuerpo}>
                    <p>Rick y Morty (en inglés: Rick and Morty) es una serie de televisión estadounidense de animación para adultos creada por Justin Roiland y Dan Harmon en 2013 para Adult Swim, también se emitió en Cartoon Network. La serie sigue las desventuras de un científico, Rick Sánchez, y su fácilmente influenciable nieto, Morty, quienes pasan el tiempo entre la vida doméstica y los viajes espaciales, temporales e intergalácticos. </p>
                    <p>Dan Harmon, el cocreador de la serie y Justin Roiland son los encargados de las voces principales de Morty y Rick, la serie también incluye las voces de Chris Parnell, Spencer Grammer y Sarah Chalke.</p> 
                </div>
                
            </div>

                </main>
            </Layout>
            
    )
}