import StateContexts from '../Context/charactersAPI/StateContexts'
import '../styles/globals.css'
function MyApp ({Component, pageProps}) {
    
    return (
    <StateContexts>
        <Component {...pageProps} />
    </StateContexts>
    )
}

export default MyApp