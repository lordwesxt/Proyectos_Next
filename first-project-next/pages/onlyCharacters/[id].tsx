import { useRouter } from "next/router";
import { useEffect, useContext, useState } from "react";
import Styles from '../../styles/OnlyCharacter.module.css'
import Layout from "@/components/Layout";
import { Loader } from "@/components/Loader";
import Link from "next/link";
import { context } from "@/Context/charactersAPI/CreateContexts";
import Grid from "@/components/Grid";
import OnlyCharacterElement from "@/components/OnlyCharacterElement";

const OnlyCharacter = () => {
  const { query: { id } } = useRouter();
    const {onlyPj, errorLoad, characterOnly} = useContext(context)
    const [isLoading, setIsLoading] = useState(true)
  useEffect(() => {
    if(id){
      characterOnly(id);
      setIsLoading(false)
    }
      
  }, [id]);
  if (!id || isLoading) {
    return (
      <Layout titulo="Personaje">
        <Loader/>
      </Layout>
    );
  }else if(errorLoad) {
    return(
      <Layout titulo="Personaje">
        <div className={Styles.content_error}>
          <h1>{errorLoad}</h1>
          <h2>Por favor, vuelva a la pagina de personajes, <Link href={'../characters/'}>Click aqui</Link></h2>
          </div>
      </Layout>
    )
  }else{
      const {gender, name} = onlyPj
      let styleBack
      if(gender === 'Male'){
      styleBack = 'name_tittle2'
      }else if (gender ==='Female'){
        styleBack = 'name_tittle3'
      }else{
        styleBack= 'name_tittle'
      }
    return (
      <Layout titulo={name}>
        <Link href={'../characters'} className={Styles.link_button}><button>Volver a personajes</button></Link>
        <span className={Styles[styleBack]}>{name}</span>
        <Grid elements={onlyPj} component={OnlyCharacterElement}/>
      </Layout>
    );
  }
  }
  

export default OnlyCharacter;
